package com.kumparantest.app.network

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}